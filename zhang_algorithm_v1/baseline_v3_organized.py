import pandas as pd
from sklearn.cluster import KMeans
import numpy as np;
    
def load_data(file):
    '''
    read in files. Some files above 200mb cannot be read
    '''
    df = pd.read_csv(file);
    return df.drop(columns = ["StartTime", "SrcAddr", "DstAddr", "State"]);
    
def add_more_column(df, new_df):
    '''
    used to append dataframe to the old dataframe
    '''
    for column in new_df:
        df[column]=new_df[column];
    
def correct_label(df):
    '''
    change the labels into correct category
    1 = bot, 0 = no bot
    '''
    lst_temp= [];
    for row in range(0, len(df["Label"])):
        value = df["Label"].iloc[row]
        if("Bot" in value):
            lst_temp.append(1)
        else:
            lst_temp.append(0);
    df["label"]=pd.Series(data = lst_temp, index = df.index);

def clean_data(df):    
    '''
    cleans dataframe, change all to floats for algorithm
    any strings are dropped or dummied, fills in nulls with zeroes
    '''
    add_more_column(df, pd.get_dummies(df["Proto"]));
    add_more_column(df, pd.get_dummies(df["Dir"]));
    correct_label(df);
    df = df.drop(columns = ["Sport", "Dport", "Proto", "Dir", "Label"])
    
    df["Dur"]=pd.to_numeric(df["Dur"], errors='coerce')
    df["sTos"] = pd.to_numeric(df["sTos"], errors='coerce')
    df["dTos"] = pd.to_numeric(df["dTos"], errors='coerce')
    df["TotPkts"] = pd.to_numeric(df["TotPkts"], errors='coerce')
    df["TotBytes"] = pd.to_numeric(df["TotBytes"], errors='coerce')
    df["SrcBytes"] = pd.to_numeric(df["SrcBytes"], errors='coerce')
    df = df.fillna(0);
    return df.copy(deep = True);

#to test if bad values
#print(np.any(np.isnan(df)))
#print(np.all(np.isfinite(df)))
def percent_one(lst):
    '''
    finds number of label that's categorized as bot
    '''
    count=0;
    for key in lst:
        if(key == 1):
            count+=1;
    print("percentage of bots in the data: "+ str(count/len(lst)*100))

def train_test_model(df):
    '''
    shuffles data for variety, train on 75%, test on 25%
    Algorithm is kmeans, we are looking for bot and nonbot so 2 clusters
    returns accuracy for both training set and testing set
    '''
    #shuffle for randomness
    df = df.sample(frac=1).reset_index(drop=True)

    size = int(len(df) * .75);
    #get a size for training and testing
    trainSet, testSet = df.iloc[:size].copy(deep=True), df.iloc[size:].copy(deep=True)

    #kmeans, find two groups
    kmeans = KMeans(n_clusters=2, random_state=0)

    #fit train to correct label
    x = trainSet.loc[:, trainSet.columns != 'label'];
    model = kmeans.fit(x, trainSet["label"]);
    y_pred = model.predict(x)
    train_accuracy = (trainSet["label"] == y_pred).sum()/len(trainSet)*100;
    
    x = testSet.loc[:, testSet.columns != 'label'];
    test_pred = model.predict(x);
    test_accuracy = (testSet["label"] == test_pred).sum()/len(testSet)*100;
    return (train_accuracy, test_accuracy);
    
def main():
    print("Began program, will definitely take a while")
    file_lst = ["capture20110815-2.binetflow","capture20110818-2.binetflow"]
    for file in file_lst:
        print("Following stats are for file: "+file);
        df = load_data(file);
        df = clean_data(df);
        percent_one(df["label"])
        #print("Example of features been fed")
        #print(df.iloc[:3])
        total_train, total_test = 0, 0;
        temp = (0, 0);
        for i in range(10):
            temp = train_test_model(df);
            total_train+=temp[0];
            total_test+=temp[1];
            
        print("Average training accuracy is: "+str(total_train/10))
        print("Average testing accuracy is: "+str(total_test/10)+"\n");
    
if __name__ == "__main__":
    main()